import React from 'react'
import { Filter } from '../App.types'
import { resetFilters } from '../utils/utils'

export const FiltersContext = React.createContext<{
  filters: Filter
  setFilters: React.Dispatch<React.SetStateAction<Filter>>
}>({
  filters: resetFilters(),
  setFilters: () => ({}),
})
