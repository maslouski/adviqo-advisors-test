import { gql } from '@apollo/client'

export const GET_ADVISORS = gql`
  {
    advisors {
      id
      name
      text
      status {
        id
        name
        slug
      }
      languages {
        id
        name
        code
      }
      reviews
    }
  }
`
export const GET_LANGUAGES = gql`
  {
    languages {
      id
      name
      code
    }
  }
`

export const GET_STATUSES = gql`
  {
    statuses {
      id
      name
      slug
    }
  }
`
