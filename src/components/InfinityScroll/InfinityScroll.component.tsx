import React, { useRef, useEffect, ReactElement } from 'react'
import { isBottom } from '../../utils/utils'

type Props = {
  onBottomHit: () => void
  isLoading: boolean
  hasMoreData: boolean
  spinner: ReactElement
}

export const InfiniteScroll: React.FC<Props> = ({
  onBottomHit,
  isLoading,
  hasMoreData,
  children,
}) => {
  const contentRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    const onScroll = () => {
      if (!isLoading && hasMoreData && isBottom(contentRef)) {
        onBottomHit()
      }
    }
    document.addEventListener('scroll', onScroll)

    return () => document.removeEventListener('scroll', onScroll)
  }, [onBottomHit, isLoading, hasMoreData])

  return <div ref={contentRef}>{children}</div>
}
