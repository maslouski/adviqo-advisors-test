import React from 'react'
import './card.style.css'
import logo from '../../logo.svg'
import { Advisor, Statuses } from '../../App.types'
import { FaLanguage } from 'react-icons/fa'
import { AiOutlineEye } from 'react-icons/ai'
import { BsToggleOn } from 'react-icons/bs'
import { BsToggleOff } from 'react-icons/bs'
import { MetaItem } from '../items/meta-item.component'

type Props = {
  item: Advisor
  handleAdvisorUpdate: (newAdvisor: Advisor) => void
}

export const Card: React.FC<Props> = ({ item, handleAdvisorUpdate }) => {
  const updateAdvisor = React.useCallback(() => {
    const newReviewsAmount = item.reviews + 1
    const newAdvisor: Advisor = { ...item, reviews: newReviewsAmount }
    handleAdvisorUpdate(newAdvisor)
  }, [item, handleAdvisorUpdate])

  return (
    <div className="card">
      <div className="card__header card__row">
        <div className="row">
          <img src={logo} className="card__image" alt="logo" />
        </div>
      </div>
      <div className="card__body card__row">
        <h4>{item.name}</h4>
        <p>{item.text}</p>
        <button className="button" onClick={updateAdvisor}>
          Review
        </button>
      </div>
      <div className="card__footer card__row">
        <ul className="card-meta ul">
          <li>
            <MetaItem
              icon={<FaLanguage color="#fff" size={20} />}
              title="Language"
              text={item.languages.code}
            />
          </li>
          <li>
            <MetaItem
              icon={
                item.status.slug === Statuses.ONLINE ? (
                  <BsToggleOn color="#33ff00" size={20} />
                ) : (
                  <BsToggleOff color="#ff4f4f" size={20} />
                )
              }
              title="Status"
              text={item.status.name}
            />
          </li>
          <li>
            <MetaItem
              icon={<AiOutlineEye color="#fff" size={20} />}
              title="Reviews"
              text={item.reviews.toString()}
            />
          </li>
        </ul>
      </div>
    </div>
  )
}
