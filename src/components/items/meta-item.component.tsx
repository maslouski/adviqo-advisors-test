import React, { ReactElement } from 'react'
import './items.style.css'

type Props = {
  icon: ReactElement
  title: string
  text: string
}

export const MetaItem: React.FC<Props> = ({ icon, title, text }) => {
  return (
    <div className="meta-item">
      <div className="meta-item__column">
        <div className="meta-item__icon">{icon}</div>
      </div>
      <div className="meta-item__column">
        <div className="meta-item__title">{title}</div>
        <div className="meta-item__desc">
          <b>{text}</b>
        </div>
      </div>
    </div>
  )
}
