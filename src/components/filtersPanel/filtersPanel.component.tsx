import React, { useCallback, useContext } from 'react'
import { SortOptions, Statuses } from '../../App.types'
import { FiltersContext } from '../../context/filtersContext.context'
import { resetFilters } from '../../utils/utils'
import { LanguagesList } from '../filters/languages.component'
import { ReviewsList } from '../filters/reviews.component'
import { StatusesList } from '../filters/statuses.component'
import './filtersPanel.style.css'

export const FiltersPanel = () => {
  const { filters, setFilters } = useContext(FiltersContext)

  const handleLanguageChange = useCallback(
    (value: string) => {
      setFilters((currentFilters) => ({
        ...currentFilters,
        languages: { ...filters.languages, code: value },
      }))
    },
    [setFilters, filters.languages]
  )

  const handleStatusChange = useCallback(
    (value: Statuses) => {
      setFilters((currentFilters) => ({
        ...currentFilters,
        status: { ...filters.status, slug: value },
      }))
    },
    [setFilters, filters.status]
  )

  const handleReviewsChange = useCallback(
    (reviews: SortOptions) => {
      setFilters((currentFilters) => ({
        ...currentFilters,
        reviews,
      }))
    },
    [setFilters]
  )

  const handleResetFilters = () => {
    setFilters(resetFilters())
  }

  return (
    <FiltersContext.Provider value={{ filters, setFilters }}>
      <div className="filters">
        <div className="filters__column">
          <p className="mb-2 filters__title">
            <b>Filters</b>
          </p>
          <div className="filters__panel">
            <LanguagesList onChange={handleLanguageChange} currentValue={filters.languages.code} />
            <StatusesList onChange={handleStatusChange} currentValue={filters.status.slug} />
          </div>
        </div>
        <div className="filters__column">
          <p className="mb-2 filters__title">
            <b>Sort by reviews</b>
          </p>
          <div className="filters__panel">
            <ReviewsList onChange={handleReviewsChange} currentValue={filters.reviews} />
          </div>
        </div>
        <div className="filters__column filters__column--expanded">
          <div className="filters__panel">
            <button className="button" onClick={handleResetFilters}>
              Reset
            </button>
          </div>
        </div>
      </div>
    </FiltersContext.Provider>
  )
}
