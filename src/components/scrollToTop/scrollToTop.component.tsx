import React from 'react'
import { AiOutlineArrowUp } from 'react-icons/ai'
import { useScrollTop } from '../../hooks/useScrollTop.hook'
import './scrollToTop.style.css'

export const ScrollToTop = () => {
  const [isVisible, scrollToTop] = useScrollTop()
  return (
    <div className="scroll-to-top">
      {isVisible && (
        <button onClick={scrollToTop}>
          <AiOutlineArrowUp size={35} color="#000" />
        </button>
      )}
    </div>
  )
}
