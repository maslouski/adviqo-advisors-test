import React from 'react'
import { useQuery } from '@apollo/client'
import { Status, Statuses } from '../../App.types'
import { GET_STATUSES } from '../../queries'

type Props = {
  onChange: (_: Statuses) => void
  currentValue: string
}

export const StatusesList: React.FC<Props> = React.memo(({ currentValue, onChange }) => {
  const { data } = useQuery(GET_STATUSES)

  const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    onChange(e.target.value as Statuses)
  }

  return (
    <select value={currentValue} onChange={handleSelectChange}>
      <option value="">All</option>
      {data?.statuses?.map((status: Status) => (
        <option key={status.id} value={status.slug}>
          {status.name}
        </option>
      ))}
    </select>
  )
})
