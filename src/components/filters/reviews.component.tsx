import React from 'react'
import { SortOptions } from '../../App.types'

type Props = {
  onChange: (_: SortOptions) => void
  currentValue: string
}

export const ReviewsList: React.FC<Props> = React.memo(({ currentValue, onChange }) => {
  const handleLanguageChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    onChange(e.target.value as SortOptions)
  }

  return (
    <select value={currentValue} onChange={handleLanguageChange}>
      <option value="">None</option>
      <option value={SortOptions.UP}>{SortOptions.UP}</option>
      <option value={SortOptions.DOWN}>{SortOptions.DOWN}</option>
    </select>
  )
})
