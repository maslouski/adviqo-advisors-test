import React from 'react'
import { useQuery } from '@apollo/client'
import { Language } from '../../App.types'
import { GET_LANGUAGES } from '../../queries'

type Props = {
  onChange: (_: string) => void
  currentValue: string
}

export const LanguagesList: React.FC<Props> = React.memo(({ currentValue, onChange }) => {
  const { data } = useQuery(GET_LANGUAGES)

  const handleLanguageChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    onChange(e.target.value)
  }

  return (
    <select value={currentValue} onChange={handleLanguageChange}>
      <option value="">All</option>
      {data?.languages?.map((language: Language) => (
        <option key={language.id} value={language.code}>
          {language.name}
        </option>
      ))}
    </select>
  )
})
