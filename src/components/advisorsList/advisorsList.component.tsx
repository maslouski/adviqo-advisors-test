import React, { useContext, useEffect, useState } from 'react'
import { useQuery } from '@apollo/client'
import { Card } from '../card/card.component'
import { Advisor } from '../../App.types'
import { ImSpinner } from 'react-icons/im'
import { InfiniteScroll } from '../InfinityScroll/InfinityScroll.component'
import './advisorsList.style.css'
import { GET_ADVISORS } from '../../queries'
import { getFilteredAdvisors } from '../../utils/utils'
import { FiltersContext } from '../../context/filtersContext.context'

export const AdvisorsList = () => {
  const { filters } = useContext(FiltersContext)
  const [advisors, setAdvisors] = React.useState<Advisor[]>([])
  const [isServerError, setIsServerError] = useState(false)

  const { loading, error, data, fetchMore } = useQuery(GET_ADVISORS, {
    notifyOnNetworkStatusChange: true,
  })

  useEffect(() => {
    if (data && data.advisors?.length) {
      setAdvisors((adv) => [...adv, ...data.advisors])
    }
  }, [data])

  const handleAdvisorUpdate = (newAdvisor: Advisor) => {
    const newAdvisors = [...advisors]
    const idx = newAdvisors.findIndex((adv) => adv.id === newAdvisor.id)
    newAdvisors[idx] = newAdvisor
    setAdvisors(newAdvisors)
  }

  const handleFetchMore = async () => {
    if (!loading) {
      try {
        await fetchMore({})
        setIsServerError(false)
      } catch (error) {
        setIsServerError(true)
      }
    }
  }

  const filteredAdvisors = getFilteredAdvisors(advisors, filters)

  return (
    <div className="advisors-container text-center">
      <InfiniteScroll
        hasMoreData={true}
        isLoading={loading}
        onBottomHit={handleFetchMore}
        spinner={<ImSpinner speed={100} size={30} className="spinner" />}
      >
        <div className="advisors-container__body">
          <ul className="advisors-list r-row r-row--wrap">
            {filteredAdvisors.map((advisor) => (
              <li key={advisor.id} className="r-column mb-2">
                <Card item={advisor} handleAdvisorUpdate={handleAdvisorUpdate} />
              </li>
            ))}
          </ul>
        </div>
        <div className="advisors-container__footer">
          {loading && <ImSpinner speed={100} size={30} className="spinner" />}
          {error && <p>Error while fetch data</p>}
          {isServerError && !loading && <p>Internal server error</p>}
          {!loading && !error && !advisors.length && <p>No advisors found</p>}
        </div>
      </InfiniteScroll>
    </div>
  )
}
