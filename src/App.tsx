import React, { useState } from 'react'
import './App.css'
import { Filter } from './App.types'
import { ScrollToTop } from './components/scrollToTop/scrollToTop.component'
import { AdvisorsList } from './components/advisorsList/advisorsList.component'
import { FiltersPanel } from './components/filtersPanel/filtersPanel.component'
import { resetFilters } from './utils/utils'
import { FiltersContext } from './context/filtersContext.context'

const App = () => {
  const [filters, setFilters] = useState<Filter>(resetFilters())
  return (
    <div className="App">
      {/* Header */}
      <header>
        <div className="container">
          <FiltersContext.Provider value={{ filters, setFilters }}>
            <FiltersPanel />
          </FiltersContext.Provider>
        </div>
      </header>

      {/* Main */}
      <main>
        <section id="advisors">
          <div className="container">
            <h2 className="section-title">Advisors</h2>
            <FiltersContext.Provider value={{ filters, setFilters }}>
              <AdvisorsList />
            </FiltersContext.Provider>
          </div>
        </section>
      </main>

      {/* Footer */}
      <footer>
        <div className="container">
          <p>Copyright Victor 2021</p>
        </div>
      </footer>

      {/* Scroll top button */}
      <ScrollToTop />
    </div>
  )
}

export default App
