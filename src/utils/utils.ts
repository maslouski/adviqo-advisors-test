import { Advisor, Filter, SortOptions } from '../App.types'

/**
 * @param advisors Advisors collection
 * @param filters Filters and sorts options
 * @returns Filtered advisors collection
 */
export const getFilteredAdvisors = (advisors: Advisor[], filters: Filter): Advisor[] => {
  return advisors
    .filter((advisor) => {
      if (filters.languages.code && advisor.languages.code !== filters.languages.code) {
        return false
      }

      if (filters.status.slug && advisor.status.slug !== filters.status.slug) {
        return false
      }

      return true
    })
    .sort((a, b) => {
      if (filters.reviews === SortOptions.UP) {
        return b.reviews - a.reviews
      }

      if (filters.reviews === SortOptions.DOWN) {
        return a.reviews - b.reviews
      }

      return 0
    })
}

/**
 * @returns Reset filters and sorts options
 */
export const resetFilters = (): Filter => ({
  status: { slug: '', name: '', id: '' },
  languages: { code: '', name: '', id: '' },
  reviews: '',
})

/**
 * Detect bottom element to trigger
 * new set of elements loading
 * on infinity scroll
 *
 * @param ref HTMLDivElement
 * @param offset Offset to bottom element in px
 * @returns Is scroll hits bottom element
 */
export const isBottom = (ref: React.RefObject<HTMLElement>, offset = 500): boolean => {
  if (!ref.current) {
    return false
  }

  return ref.current.getBoundingClientRect().bottom <= window.innerHeight + offset
}
