import React from 'react'
import { useRef } from 'react'
import { Filter } from '../App.types'
import { getFilteredAdvisors, resetFilters, isBottom } from './utils'

describe('Utils', () => {
  describe('getFilteredAdvisors', () => {
    it('should return filtered array which should be defined', async () => {
      const filter: Filter = {
        status: { slug: '', name: '', id: '' },
        languages: { code: '', name: '', id: '' },
        reviews: '',
      }
      const test = getFilteredAdvisors([], filter)
      expect(test).toBeDefined()
      expect(test).toBeInstanceOf(Array)
    })
  })

  describe('resetFilters', () => {
    it('should return filtered object which should be defined', async () => {
      const filter: Filter = {
        status: { slug: '', name: '', id: '' },
        languages: { code: '', name: '', id: '' },
        reviews: '',
      }
      const test = resetFilters()
      expect(test).toBeDefined()
      expect(test).toEqual(filter)
    })
  })
})
