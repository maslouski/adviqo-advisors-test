const express = require('express')
const cors = require('cors')
const { graphqlHTTP } = require('express-graphql')
const gql = require('graphql-tag')
const { buildASTSchema } = require('graphql')
const faker = require('faker')

const schema = buildASTSchema(gql`
  type Query {
    advisors: [Advisor]
    languages: [Language]
    statuses: [Status]
  }

  type Advisor {
    id: ID
    name: String
    text: String
    status: Status
    languages: Language
    reviews: Int
  }

  type Language {
    id: ID
    name: String
    code: String
  }

  type Status {
    id: ID
    name: String
    slug: String
  }
`)

const languages = [
  { id: faker.datatype.uuid(), name: 'English', code: 'en' },
  { id: faker.datatype.uuid(), name: 'German', code: 'ge' },
  { id: faker.datatype.uuid(), name: 'French', code: 'fr' },
  { id: faker.datatype.uuid(), name: 'Russian', code: 'ru' },
]

const statuses = [
  { id: faker.datatype.uuid(), name: 'Online', slug: 'online' },
  { id: faker.datatype.uuid(), name: 'Offline', slug: 'offline' },
]

const root = {
  advisors: async () => {
    // Imitate network connection.
    // Set timeout for 1-5 seconds.
    const seconds = faker.datatype.number({min: 1, max: 5}) * 1000
    await new Promise(resolve => setTimeout(resolve, seconds))

    // Return collection of fake advisors.
    return Array(10).fill(null).map(() => ({
      id: faker.datatype.uuid(),
      name: faker.name.findName(),
      text: faker.lorem.sentence(),
      status: statuses[faker.datatype.number({min: 0, max: statuses.length - 1})],
      languages: languages[faker.datatype.number({min: 0, max: languages.length - 1})],
      reviews: faker.datatype.number({min: 0, max: 100}),
    }))
  },
  languages,
  statuses,
}

const app = express()
app.use(cors())
app.use('/graphql', graphqlHTTP({
  schema,
  rootValue: root,
  graphiql: true,
}))

const port = process.env.PORT || 4000
app.listen(port)
console.log(`Running a GraphQL API server at localhost:${port}/graphql`)