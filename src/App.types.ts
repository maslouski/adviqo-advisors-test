export interface Advisor {
  id: string
  name: string
  text: string
  status: Status
  languages: Language
  reviews: number
}

export interface Filter {
  status: Status
  languages: Language
  reviews: SortOptions | ''
}

export enum Statuses {
  ONLINE = 'online',
  OFFLINE = 'offline',
}

export enum SortOptions {
  UP = 'up',
  DOWN = 'down',
}

export enum LanguageCodes {
  ENGLISH = 'en',
  FRENCH = 'fr',
  GERMAN = 'ge',
  RUSSIAN = 'ru',
}

export interface Status {
  id: string
  name: string
  slug: Statuses | string
}

export interface Language {
  id: string
  name: string
  code: LanguageCodes | string
}
