import { useState, useEffect } from 'react'

/**
 * @returns isVisible toggle and scrollToTop action
 */
export const useScrollTop = (): [boolean, () => void] => {
  const [isVisible, setIsVisible] = useState(false)

  useEffect(() => {
    document.addEventListener('scroll', toggleVisibility)

    return () => document.removeEventListener('scroll', toggleVisibility)
  }, [isVisible])

  const toggleVisibility = () => {
    window.pageYOffset > 300 ? setIsVisible(true) : setIsVisible(false)
  }

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }

  return [isVisible, scrollToTop]
}
