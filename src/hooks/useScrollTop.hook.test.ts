import { renderHook } from '@testing-library/react-hooks'
import { useScrollTop } from './useScrollTop.hook'

describe('Hooks', () => {
  describe('useScrollTop', () => {
    it('isVisible should return false on init', async () => {
      const test = renderHook(() => useScrollTop())
      const [isVisible] = test.result.current

      expect(isVisible).toBe(false)
    })
  })
})
