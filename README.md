# Getting Started with Adviqo test application

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


The project is a simple React application which shows a list of advisors and sort them based on whether or not an advisor is online or offline and which language an advisor speaks. It also sorts the list of advisors by the number of reviews he/she received.


The list of advisors is getting from a GraphQL API server which is running locally. Server uses [faker](https://www.npmjs.com/package/faker) package to generate simple advisor profile and also simulates a delay in returning response through a network.


Application scope:
- Sort advisors by number of reviews;
- Filter advisors by status and language;
- Use of a node.js server to serve data to the client;
- Use of typescript;
- Communication with backend via graphql;
- Unlimited scrolling of advisors when user scrolls to the bottom of the page;
- Support IE11.

## Available Scripts

In the project directory, you can run:

### `yarn start:web`

Runs the web app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn start:server`

Runs the GraphQL API server in the development mode.\
Open [http://localhost:4000/graphql](http://localhost:4000/graphql) to view it in the browser.

### `yarn watch:server`

Runs the GraphQL API server in the development mode.\
Open [http://localhost:4000/graphql](http://localhost:4000/graphql) to view it in the browser.

The page will reload if you make edits.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn lint`

Launches the eslint runner.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
